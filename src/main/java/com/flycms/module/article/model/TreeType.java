package com.flycms.module.article.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Open source house, All rights reserved
 * 版权：28844.com<br/>
 * 开发公司：28844.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 19:46 2018/9/22
 */
@Setter
@Getter
public class TreeType implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer nodeid;
    private Integer fatherId;
    private String text;
    private List<TreeType> nodes;
}
