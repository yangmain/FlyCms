package com.flycms.module.article.service;

import com.flycms.core.entity.DataVo;
import com.flycms.module.article.dao.ArticleCategoryDao;
import com.flycms.module.article.model.ArticleCategory;
import com.flycms.module.article.model.TreeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Open source house, All rights reserved
 * 版权：28844.com<br/>
 * 开发公司：28844.com<br/>
 *
 * 文章分类处理服务类
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 15:35 2018/9/18
 */
@Service
public class ArticleCategoryService {

    @Autowired
    protected ArticleCategoryDao articleCategoryDao;

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    @Transactional
    public DataVo addArticleCategory(Integer pid,String name){
        DataVo data = DataVo.failure("操作失败");
        if(this.checkArticleCategoryByName(name,null)){
            return data = DataVo.failure("分类名称不得重复");
        }
        ArticleCategory category=new ArticleCategory();
        category.setFatherId(pid);
        category.setName(name);
        articleCategoryDao.addArticleCategory(category);
        if(category.getId()>0){
            Map<String, Object> map=new HashMap<>();
            map.put("id", category.getId());
            map.put("pId", category.getFatherId());
            map.put("name", category.getName());
            data = DataVo.success("添加成功",map);
        }else{
            data = DataVo.failure("添加失败！");
        }
        return data;
    }

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    @Transactional
    public DataVo deleteArticleCategoryById(Integer id) {
        DataVo data = DataVo.failure("操作失败");
        int totalCount =articleCategoryDao.deleteArticleCategoryById(id);
        if(totalCount>0){
            data = DataVo.success("删除成功！");
        }else{
            data = DataVo.failure("删除失败！");
        }
        return data;
    }
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    @Transactional
    public DataVo editArticleCategoryById(Integer id,String name){
        DataVo data = DataVo.failure("操作失败");
        if(this.checkArticleCategoryByName(name,id)){
            return data = DataVo.failure("分类名称已存在！");
        }
        ArticleCategory category=new ArticleCategory();
        category.setId(id);
        category.setName(name);
        articleCategoryDao.editArticleCategoryById(category);
        if(category.getId()>0){
            data = DataVo.success("修改成功");
        }else{
            data = DataVo.failure("添加失败！");
        }
        return data;
    }

    //拖拽操作保存分类归属和排序
    @Transactional
    public DataVo editCategoryDragsById(Integer id,Integer pId){
        DataVo data = DataVo.failure("操作失败");
        if(this.checkCategoryById(id,0)){
            return data = DataVo.failure("分类不存在！");
        }
        System.out.println("----------------"+pId);
        ArticleCategory category=new ArticleCategory();
        category.setId(id);
        category.setFatherId(pId);
        articleCategoryDao.editArticleCategoryById(category);
        if(category.getId()>0){
            data = DataVo.success("修改成功");
        }else{
            data = DataVo.failure("添加失败！");
        }
        return data;
    }

    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    //按id查询分类信息
    public ArticleCategory findCategoryById(Integer id,Integer status){
        return articleCategoryDao.findCategoryById(id,status);
    }

    /**
     * 查询分类名称是否已存在！
     *
     * @param id
     *         查询id
     * @param status
     *         审核状态
     * @return
     */
    public boolean checkCategoryById(Integer id,Integer status) {
        return articleCategoryDao.findCategoryById(id,status)==null;
    }

    /**
     * 查询分类名称是否已存在！
     *
     * @param name
     *         分类名称
     * @return
     */
    public boolean checkArticleCategoryByName(String name,Integer id) {
        int totalCount = articleCategoryDao.checkArticleCategoryByName(name,id);
        return totalCount > 0 ? true : false;
    }

    //根据分类id查询所属的所有子类
    public List<ArticleCategory> getCategoryListByFatherId(Integer fatherId){
        return articleCategoryDao.getCategoryListByFatherId(fatherId);
    }

    //查询所有分类
    public List<ArticleCategory> getCategoryAllList(){
        return articleCategoryDao.getCategoryAllList();
    }


    public List getInfoEmonChild(List<ArticleCategory> list){
        List<Map> listMap = new ArrayList();

        List<String> listString = new ArrayList();
        listString.add("jjjj");
        listString.add("eeee");
        for(ArticleCategory category :list){
            List<ArticleCategory> categorylist =this.getCategoryListByFatherId(category.getId());//把父类的编号传入,查询改父亲下的子类
            Map tempMap = new HashMap();
            tempMap.put("nodeId", category.getId());
            tempMap.put("text", category.getName());
            tempMap.put("href", "#node-1");
            tempMap.put("selectable", true);
            tempMap.put("id", category.getId());
            tempMap.put("selected", true);
            tempMap.put("tags", listString);
            if(categorylist.size()>0){//如果该设备还有孩子,继续做查询,直到设备没有孩子,也就是最后一个节点
                tempMap.put("nodes", getInfoEmonChild(categorylist));
            }

            listMap.add(tempMap);
        }
        return listMap;
    }
}
