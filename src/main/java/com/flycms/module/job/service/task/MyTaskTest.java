package com.flycms.module.job.service.task;

import com.flycms.module.weight.service.WeightService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author 郑杰
 * @date 2018/10/06 10:03:43
 */
@Slf4j
@Component
public class MyTaskTest {
    @Autowired
    private WeightService weightService;

    public void test(String params) {
        System.out.println("--------------------1---------------------");
        log.info("我是带参数的test方法，正在被执行，参数为：{}" , params);
    }

    public void test1() {
        System.out.println("--------------------2---------------------");
        weightService.updateArticleWeight();
        log.info("我是不带参数的test1方法，正在被执行");
    }
}
