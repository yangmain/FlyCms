package com.flycms.module.job.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.Date;

/**
 * @author 郑杰
 * @date 2018/10/05 19:47:38
 */
@Setter
@Getter
public class JobLog implements Serializable {

    private Integer id;

    /**
     * 任务id
     */
    private Integer jobId;

    /**
     * Bean名称
     */
    private String beanName;

    /**
     * 方法名称
     */
    private String methodName;

    /**
     * 参数
     */
    private String params;

    /**
     * cron表达式
     */
    private String cronExpression;

    /**
     * 0=成功,1=失败
     */
    private String status;

    /**
     * 错误信息
     */
    private String errorMsg;

    /**
     * 耗时（毫秒）
     */
    private Long times;

    /**
     * 创建日期
     */
    private Date createTime;

    @Override
    public String toString() {
        return "JobLog{" +
                "id=" + id +
                ", jobId=" + jobId +
                ", beanName='" + beanName + '\'' +
                ", methodName='" + methodName + '\'' +
                ", params='" + params + '\'' +
                ", cronExpression='" + cronExpression + '\'' +
                ", status='" + status + '\'' +
                ", error='" + errorMsg + '\'' +
                ", times=" + times +
                ", createTime=" + createTime +
                '}';
    }
}
