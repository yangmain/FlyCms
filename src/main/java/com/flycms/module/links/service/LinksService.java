/*
 *	Copyright © 2015 Zhejiang SKT Science Technology Development Co., Ltd. All rights reserved.
 *	浙江斯凯特科技发展有限公司 版权所有
 *	http://www.28844.com
 *
 */

package com.flycms.module.links.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.flycms.core.entity.DataVo;
import com.flycms.core.entity.PageVo;
import com.flycms.module.links.dao.LinksDao;
import com.flycms.module.links.model.Links;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 友情链接服务
 * 
 * @author sunkaifei
 * 
 */
@Service
public class LinksService {
	
	@Autowired
	private LinksDao linksDao;
	
	// ///////////////////////////////
	// ///// 增加 ////////
	// ///////////////////////////////		
	
	/**
	 * 添加网站友情链接
	 *
	 * @param links
	 * @return
	 * @throws Exception
	 */
	public DataVo addLinks(Links links) {
		DataVo data = DataVo.failure("操作失败");
		if(this.checkLinksByLinkUrl(links.getLinkUrl())){
			return DataVo.failure("网站连接已存在！");
		}
		links.setCreateTime(new Date());
		int totalCount=linksDao.addLinks(links);
		if(totalCount > 0){
			data = DataVo.success("添加成功", DataVo.NOOP);
		}else{
			data=DataVo.failure("未知错误！");
		}
		return data;
	}
	
	// ///////////////////////////////
	// ///// 刪除 ////////
	// ///////////////////////////////
	/**
	 * 按id删除友情链接信息
	 *
	 * @param id
	 * @return
	 */
	public boolean deleteLinksById(Integer id) {
		int totalCount=linksDao.deleteLinksById(id);
		return totalCount > 0 ? true : false;
	}
	
	// ///////////////////////////////
	// ///// 修改 ////////
	// ///////////////////////////////
	/**
	 * 修改友情链接信息
	 *
	 * @param links
	 * @return
	 */
	public DataVo updateLinksById(Links links) {
		DataVo data = DataVo.failure("操作失败");
		links.setCreateTime(new Date());
		int totalCount=linksDao.updateLinksById(links);
		if(totalCount > 0){
			data = DataVo.success("添加成功", DataVo.NOOP);
		}else{
			data=DataVo.failure("未知错误！");
		}
		return data;
	}
	
	// ///////////////////////////////
	// ///// 查询 ////////
	// ///////////////////////////////
	/**
	 * 按id查询友情链接信息
	 *
	 * @param id
	 * @return
	 */
	public Links findLinksById(Integer id){
		return linksDao.findLinksById(id);
	}

	/**
	 * 查询网站链接是否存在
	 *
	 * @param linkUrl
	 *         网站链接地址
	 * @return
	 */
	public boolean checkLinksByLinkUrl(String linkUrl) {
		int totalCount = linksDao.checkLinksByLinkUrl(linkUrl);
        return totalCount > 0 ? true : false; 
	}
	
	/**
	 * @return
	 * @throws Exception
	 */
	public int getLinksCount(long linkType, long show) throws Exception {
		return linksDao.getLinksCount(linkType, show);
		
	}
	
	/**
	 * @param offset
	 * @param rows
	 * @return
	 * @throws Exception
	 */
	public List<Links> getLinksList(int linkType, int show, int offset, int rows) throws Exception {
		List<Links> friendLinkList = linksDao.getLinksList(linkType, show,offset, rows);
		return friendLinkList;
	}
	
	/**
	 * @param pageNum
	 * @return
	 * @throws Exception
	 */
	public PageVo<Links> getLinksListPage(int linkType, int show, int pageNum, int rows) throws Exception {
		PageVo<Links> pageVo = new PageVo<Links>(pageNum);
		pageVo.setRows(rows);
		pageVo.setList(this.getLinksList(linkType, show, pageVo.getOffset(), pageVo.getRows()));
		pageVo.setCount(this.getLinksCount(linkType,show));
		return pageVo;
	}
}
